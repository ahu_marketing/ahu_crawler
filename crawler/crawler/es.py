# -*- coding: utf-8 -*-

import json
import urllib3
import logging
import datetime

logging.basicConfig(filename='../es.log',level=logging.WARNING)

es_url = "http://localhost:9200/documents/doc"
es_bulk_url = "http://localhost:9200/_bulk"
now = str(datetime.datetime.now())

user_agent = {'user-agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0'}
http = urllib3.PoolManager(maxsize=20,headers=user_agent,cert_reqs='CERT_NONE')

def store_in_es(src,item,bulk):
	if bulk:
		query = build_bulk_query(item,src)
		#log query here
		# logging.warning(query)

		data = http.urlopen('POST', es_bulk_url, headers={'Content-Type':'application/json'}, body=query).data
		# log data
		# logging.warning(" ".join(data))
	else:
		post_data = {}
		post_data["name"] = item['name']
		post_data["category"] = item['category']
		post_data["author"] = item['author']
		post_data["description"] = item['description']
		post_data["ahu_download_url"] = item['ahu_download_url']
		post_data["src_download_url"] = item['src_download_url']
		post_data["document_url"] = item['document_url']
		post_data["source"] = src

		json_data = json.dumps(post_data)

		data = http.urlopen('POST', es_url, headers={'Content-Type':'application/json'}, body=json_data).data
		data = json.loads(data)
		created = False
		try:
			created = data['created']
			doc_id = data['_id']

		except Exception, e:
			logging.warning(now+" : [elasticsearch]["+src+"] "+data['error'])
		else:
			pass

def build_bulk_query(items,src):

	query = ""
	for item in items:
		query = query + '{"index":{ "_index" : "documents","_type" : "doc" } }\n{"name":'+json.dumps(item['name'])+',"category":'+json.dumps(item['category'])+',"author":'+json.dumps(item['author'])+',"description":'+json.dumps(item['description'])+',"ahu_download_url":"'+item['ahu_download_url']+'","src_download_url":'+json.dumps(item['src_download_url'])+',"document_url":'+json.dumps(item['document_url'])+',"source":'+json.dumps(src)+'}\n'
	return query