# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class KantakjiItem(scrapy.Item):
	category = scrapy.Field()
	name = scrapy.Field()
	author = scrapy.Field()
	description = scrapy.Field()
	document_url = scrapy.Field()
	src_download_url = scrapy.Field()
	ahu_download_url = scrapy.Field()

class IslamPortItem(scrapy.Item):
	category = scrapy.Field()
	name = scrapy.Field()
	author = scrapy.Field()
	description = scrapy.Field()
	document_url = scrapy.Field()
	src_download_url = scrapy.Field()
	ahu_download_url = scrapy.Field()

class AlmaktabaItem(scrapy.Item):
	category = scrapy.Field()
	name = scrapy.Field()
	author = scrapy.Field()
	description = scrapy.Field()
	document_url = scrapy.Field()
	src_download_url = scrapy.Field()
	ahu_download_url = scrapy.Field()

class WaqfeyaItem(scrapy.Item):
	category = scrapy.Field()
	name = scrapy.Field()
	author = scrapy.Field()
	description = scrapy.Field()
	document_url = scrapy.Field()
	src_download_url = scrapy.Field()
	ahu_download_url = scrapy.Field()

class AlmeshkatItem(scrapy.Item):
	category = scrapy.Field()
	name = scrapy.Field()
	author = scrapy.Field()
	description = scrapy.Field()
	document_url = scrapy.Field()
	src_download_url = scrapy.Field()
	ahu_download_url = scrapy.Field()

class IefpediaItem(scrapy.Item):
	category = scrapy.Field()
	name = scrapy.Field()
	author = scrapy.Field()
	description = scrapy.Field()
	document_url = scrapy.Field()
	src_download_url = scrapy.Field()
	ahu_download_url = scrapy.Field()

class KalamullahItem(scrapy.Item):
	category = scrapy.Field()
	name = scrapy.Field()
	author = scrapy.Field()
	description = scrapy.Field()
	document_url = scrapy.Field()
	src_download_url = scrapy.Field()
	ahu_download_url = scrapy.Field()

class IslamHouseItem(scrapy.Item):
	category = scrapy.Field()
	name = scrapy.Field()
	author = scrapy.Field()
	description = scrapy.Field()
	document_url = scrapy.Field()
	src_download_url = scrapy.Field()
	ahu_download_url = scrapy.Field()

class IslamMessageItem(scrapy.Item):
	category = scrapy.Field()
	name = scrapy.Field()
	author = scrapy.Field()
	description = scrapy.Field()
	document_url = scrapy.Field()
	src_download_url = scrapy.Field()
	ahu_download_url = scrapy.Field()