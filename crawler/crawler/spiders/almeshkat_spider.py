# -*- coding: utf-8 -*-

import scrapy
from crawler.items import AlmeshkatItem
from crawler.es import store_in_es

class AlmeshkatSpider(scrapy.Spider):
	name = "almeshkat"
	allowed_domains = ["almeshkat.net"]
	start_urls = [
		"http://www.almeshkat.net/books/index.php"
	]

	def parse(self, response):
		i = 0
		cats_url = response.xpath('//a[contains(@href,"list.php?cat=")]/@href').extract()
		for url in cats_url:
			if "cat=67" in url: # this cat has books with other languages 
				pass
			else:
				i = i + 1
				url = "http://www.almeshkat.net/books/"+url.encode('utf-8','ignore')
				if i<2:
					yield scrapy.Request(url, callback=self.parse_categories_contents)

	def parse_categories_contents(self, response):
		j = 0
		book_urls = response.xpath('//tr[@valign="middle"]/td[1]/p/font/a/@href').extract()
		for url in book_urls:
			j = j + 1
			url = "http://www.almeshkat.net/books/"+url.encode('utf-8','ignore')
			if j<2:
				yield scrapy.Request(url, callback=self.parse_books_contents)
			
	def parse_books_contents(self, response):
		category = ""
		if response.xpath('//td/span/a[@href="index.php"]/following-sibling::a/text()'):
			category = response.xpath('//td/span/a[@href="index.php"]/following-sibling::a/text()').extract()[0].encode('utf-8','ignore')
		
		name = ""
		author = ""
		description = ""
		download_url = ""
		document_url = response.url

		tds =  response.xpath('//td/span/a[@href="index.php"]/parent::span/parent::td/parent::tr/following-sibling::tr/td[@width="10%"]')
		for td in tds:
			label = td.xpath('font/text()').extract()[0].encode('utf-8','ignore')
			if "التحميل" in label:
				download_url = td.xpath('following-sibling::td/a/@href').extract()[0].encode('utf-8','ignore')

			if "نبذه" in label:
				description = td.xpath('following-sibling::td/font/p/text()').extract()[0].encode('utf-8','ignore')

			if td.xpath('following-sibling::td/font/text()'):
				value = td.xpath('following-sibling::td/font/text()').extract()[0].encode('utf-8','ignore')
				if "العنوان" in label:					
					name = value
				if "المؤلف" in label:
					author = value

		item = AlmeshkatItem()
		item['category'] = category
		item['name'] = name
		item['author'] = author
		item['description'] = description
		item['ahu_download_url'] = "pending"
		item['document_url'] = document_url
		item['src_download_url'] = download_url

		store_in_es("almeshkat.net",item,False)