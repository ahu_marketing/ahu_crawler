# -*- coding: utf-8 -*-

import scrapy
from crawler.items import IslamPortItem
from crawler.es import store_in_es

class IslamPortSpider(scrapy.Spider):
	name = "islamport_recent"
	allowed_domains = ["islamport.com"]
	start_urls = [
		"http://islamport.com/new_b.html"
	]

	def parse(self, response):
		categories = response.xpath('//p/font[@face]/b/text()').extract()
		data = response.xpath('//font[@face]/b/ancestor::p/following-sibling::font')
		i = 1
		items = []
		for category in categories:
			cat = category.encode('utf-8','ignore').split('-')[1].strip()
			rows = data.xpath('table['+str(i)+']/tr')
			i = i + 1
			for row in rows:
				download_link = ""
				name = row.xpath('td[1]/font/ul/b/font/text()').extract()[0].encode('utf-8','ignore')
				meta = row.xpath('td[1]/font/ul/ul/div').extract()[0].encode('utf-8','ignore')
				if row.xpath('td[2]/p/a/@href'):
					download_link = row.xpath('td[2]/p/a/@href').extract()[0].encode('utf-8','ignore')
				elif row.xpath('td[2]/p/font/a/@href'):
					download_link = row.xpath('td[2]/p/font/a/@href').extract()[0].encode('utf-8','ignore')
				if download_link != "":
					meta = meta.replace('<div dir="rtl" align="right">','')
					meta = meta.replace('</div>','')
					meta = meta.split("<br>")
					description = ' - '.join(meta)
					description = description.strip()
					author = ""
					if len(meta)>2: #extract the author name
						for m in meta:
							if "المؤلف" in m:
								author = m.replace("المؤلف :","")
								author = m.replace("المؤلف:","")
								author = author.strip()
								description = description.replace("المؤلف :"+author,"").strip()

					item = IslamPortItem()
					item['category'] = cat
					item['name'] = name
					item['author'] = author
					item['description'] = description
					item['ahu_download_url'] = "pending"
					item['document_url'] = response.url
					item['src_download_url'] = download_link
					items.append(item)

		if len(items)>0:
			store_in_es("islamport.com",items,True)