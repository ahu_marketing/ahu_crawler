# -*- coding: utf-8 -*-

import scrapy
from crawler.items import KantakjiItem
from crawler.es import store_in_es

class KantakjiSpider(scrapy.Spider):
	name = "kantakji"
	allowed_domains = ["kantakji.com"]
	start_urls = [
		"http://kantakji.com/"
	]

	def parse(self, response):
		categories = response.xpath('//a[@class="categoryLink"]')
		for category in categories:
			url = "http://kantakji.com"+category.xpath('@href').extract()[0]
			yield scrapy.Request(url, callback=self.parse_categories_contents)

	def parse_categories_contents(self, response):
		category = response.xpath('//div[@id="title"]/h1/text()').extract()[0].encode('utf-8','ignore').strip()
		documents = response.xpath('//li[@class="liArticle"]')
		items = []
		# j = 0
		for document in documents:
			# if j<1:
			# 	j = j+1
			store = True
			item = KantakjiItem()
			item['category'] = category
			name_node = document.xpath('h3')
			item['name'] = name_node.xpath('a/text()').extract()[0].encode('utf-8','ignore').strip()
			item['author'] = document.xpath('//span[@class="cat_author"]/text()').extract()[0].encode('utf-8','ignore').strip()
			item['description'] = document.xpath('div[@class="articleDesc"]/text()').extract()[0].encode('utf-8','ignore').strip()
			item['ahu_download_url'] = "pending"
			item['document_url'] = "http://kantakji.com"+name_node.xpath('a/@href').extract()[0].encode('utf-8','ignore').strip()
			if document.xpath('div[@class="articleDesc"]/div[@class="links"]/a/@href'):
				item['src_download_url'] = "http://kantakji.com"+document.xpath('div[@class="articleDesc"]/div[@class="links"]/a/@href').extract()[0].encode('utf-8','ignore').strip()
			else:
				store = False
			if store:
				items.append(item)

		# check if items has any item in it
		if len(items)>0:
			# store links to es
			store_in_es("kantakji.com",items,True)