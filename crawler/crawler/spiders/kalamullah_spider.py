# -*- coding: utf-8 -*-

import scrapy
from crawler.items import KalamullahItem
import redis
import hashlib
import logging
import datetime
from crawler.es import store_in_es

logging.basicConfig(filename='../ahu_crawler.log',level=logging.WARNING)
now = str(datetime.datetime.now())

class KalamullahSpider(scrapy.Spider):
	name = "kalamullah"
	allowed_domains = ["kalamullah.com"]
	start_urls = [
		"http://kalamullah.com/"
	]

	def parse(self, response):
		categories = response.xpath('//div[@class="menu1-level2-no"]')
		for category in categories :
			url = "http://kalamullah.com/"+category.xpath('a/@href').extract()[0]
			yield scrapy.Request(url, callback=self.parse_category_content)

	def parse_category_content(self, response):
		category = response.url.replace('http://kalamullah.com/','').replace('.html','').replace('-','.')
		containers = response.xpath('//div[@id="colNormal"]/table/tr/td[@bgcolor="#FFFFFF"]')
		
		tracker = containers[0].xpath('.//h2[@class="sidetitle"]/text()').extract()[0]
		tracker_hash = hashlib.md5(tracker).hexdigest()
		r = redis.StrictRedis(host='localhost', port=6379, db=8, socket_connect_timeout=2)
		i = 0
		src_hash = r.get('kalamullah:'+category)
		
		for elem in containers:
			if i == 0 and tracker_hash == src_hash: #no new links has been found
				break
			else: #there are new links
				if i == 0: #save the hash of first link
					if not r.set('kalamullah:'+category,tracker_hash):
						logging.warning(now+" couldnt set the hash '"+tracker_hash+"' to source kalamullah")
						break
				
				if tracker_hash == src_hash: #we have reached the old links stop the loop
					break
				else:
					if elem.xpath('.//ul/li/a/@href'):
						download_url = "http://kalamullah.com/"+elem.xpath('.//ul/li/a/@href').extract()[0]
						download_link_label = elem.xpath('.//ul/li/a/text()').extract()[0]

						category = category.replace('.',' ').strip()
						author = ""


						if elem.xpath('.//p/b/text()'):
							author = elem.xpath('.//p/b/text()').extract()[0].split('|')[0].replace('Author:','').strip()

						if "Download" in download_link_label:
							if (".pdf" or ".doc" or ".ppt" or ".chm") in download_url:
								name = elem.xpath('.//h2[@class="sidetitle"]/text()').extract()[0].strip()
								description = elem.xpath('.//ul/preceding-sibling::p[1]/text()').extract()[0].strip()
								document_url = response.url

								item = KalamullahItem()
								item['category'] = category
								item['name'] = name
								item['author'] = author
								item['description'] = description
								item['document_url'] = document_url
								item['ahu_download_url'] = "pending"
								item['src_download_url'] = download_url
								store_in_es("kalamullah.com",item,False)

							else: #follow the page maybe there is a download link in it
								request = scrapy.Request(download_url, callback=self.parse_book_page) 
							 	request.meta['category'] = category
							 	request.meta['author'] = author
								yield request
					
			i = i+1
			
	def parse_book_page(self, response):
		category = response.meta['category']
		author = response.meta['author']

		# test if there is a download link & parse it
		if response.xpath('//ul/li/a/@href'):
			download_url = response.xpath('//ul/li/a/@href').extract()[0]
			download_link_label = response.xpath('//ul/li/a/text()').extract()[0]
			if "Download" in download_link_label:
				if (".pdf" or ".doc" or ".ppt" or ".chm") in download_url:
					if elem.xpath('h1/text()'):
						name = elem.xpath('h1/text()').extract()[0].strip()
						author = ""
						description = response.xpath('//ul/preceding-sibling::p[1]/text()').extract()[0].strip()
						document_url = response.url

						item = KalamullahItem()
						item['category'] = category
						item['name'] = name
						item['author'] = author
						item['description'] = description
						item['document_url'] = document_url
						item['ahu_download_url'] = "pending"
						item['src_download_url'] = download_url
						store_in_es("kalamullah.com",item,False)