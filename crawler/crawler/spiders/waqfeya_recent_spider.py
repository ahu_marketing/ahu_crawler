# -*- coding: utf-8 -*-

import scrapy
from crawler.items import WaqfeyaItem
import redis
import hashlib
import logging
import datetime
from crawler.es import store_in_es

logging.basicConfig(filename='../ahu_crawler.log',level=logging.WARNING)
now = str(datetime.datetime.now())

class WaqfeyaSpider(scrapy.Spider):
	name = "waqfeya_recent"
	allowed_domains = ["waqfeya.com"]
	start_urls = [
		"http://waqfeya.com/"
	]

	def parse(self, response):
		r = redis.StrictRedis(host='localhost', port=6379, db=8, socket_connect_timeout=2)
		i = 0
		rows = response.xpath('//td[@class="row1"][3]/span[@class="postbody"]/ul/li/a')
		src_hash = r.get('waqfeya:recent')

		for row in rows:
			url = "http://waqfeya.com/"+row.xpath('@href').extract()[0].encode('utf-8','ignore').strip()
			url_hash = hashlib.md5(url).hexdigest()
			if i == 0 and url_hash == src_hash: #no new links has been found
				break
			else: #there are new links
				if i == 0: #save the hash of first link
					if not r.set('waqfeya:recent',url_hash):
						logging.warning(now+" couldnt set the hash '"+url_hash+"' to source waqfeya")
						break
				if url_hash == src_hash: #we have reached the old links stop the loop
					break
				else:
					yield scrapy.Request(url, callback=self.parse_recent_books)
			i = i+1

	def parse_recent_books(self, response):
		td = response.xpath('//tr[@valign="top"]/td[2]')
		category = response.xpath('//span[@class="cattitle"]/a[last()]/text()').extract()[1].encode('utf-8','ignore')
		category = category.split(" ")
		category.pop(0)
		category = ' '.join(category)
		description = ""
		document_url = response.url
		name = response.xpath('//title/text()').extract()[0].encode('utf-8','ignore')
		name = name.replace("- المكتبة الوقفية للكتب المصورة PDF","")

		table2 = td.xpath('table[2]')
		a = table2.xpath('tbody/tr/td/span/ul/li[last()]/a[1]')
		dld_title = a.xpath('text()').extract()[0].encode('utf-8','ignore')
		download_url = a.xpath('@href').extract()[0].encode('utf-8','ignore')
		if "تحميل" in dld_title or "Save Target As" in dld_title or ("الكتاب" in dld_title and (".pub" in download_url or ".chm" in download_url or ".doc" in download_url) ): #if this is a download link
			#check for the author
			lis = table2.xpath('tbody/tr/td/span/ul/li')
			author = ""
			for li in lis:
				li_content = li.xpath('text()').extract()[0].encode('utf-8','ignore')
				if "المؤلف" in li_content:
					author = li_content.replace("المؤلف :","")
					author = li_content.replace("المؤلف:","")
					author = author

			item = WaqfeyaItem()
			item['category'] = category.strip()
			item['name'] = name.strip()
			item['author'] = author.strip()
			item['description'] = description
			item['ahu_download_url'] = "pending"
			item['document_url'] = document_url
			item['src_download_url'] = download_url

			# store links to es
			store_in_es("waqfeya.com",item,False)