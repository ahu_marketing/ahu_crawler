# -*- coding: utf-8 -*-

import scrapy
from crawler.items import AlmeshkatItem
import redis
import hashlib
import logging
import datetime
from crawler.es import store_in_es
import re

logging.basicConfig(filename='../ahu_crawler.log',level=logging.WARNING)
now = str(datetime.datetime.now())

class AlmeshkatSpider(scrapy.Spider):
	name = "almeshkat_recent"
	allowed_domains = ["almeshkat.net"]
	start_urls = [
		"http://www.almeshkat.net/books/index.php"
	]

	def parse(self, response):

		r = redis.StrictRedis(host='localhost', port=6379, db=8, socket_connect_timeout=2)
		i = 0
		# rows = response.xpath('//td[@width="20%"]/table[last()]/tr[2]/td/table[2]/tr/td/p/a')
		raw_data = response.xpath('body/center/table/tr[last()]/td[last()]/table/tr/td[@width="20%"]/table[last()]/tr[2]/td[@width="100%"]/table[last()]').extract()[0].encode('utf-8','ignore').strip()
		data = raw_data.split('آخر الكتب إضافة')[1]

		url_list = []
		data = data.replace('&amp;','&')
		rx = re.compile("open.php\?cat=\d*&book=\d+",re.IGNORECASE)
		url_list = rx.findall(data)

		src_hash = r.get('almeshkat:recent')

		for url in url_list:
			url = "http://www.almeshkat.net/books/"+url			
			url_hash = hashlib.md5(url).hexdigest()
			if i == 0 and url_hash == src_hash: #no new links has been found
				break
			else: #there are new links
				if i == 0: #save the hash of first link
					if not r.set('almeshkat:recent',url_hash):
						logging.warning(now+" couldnt set the hash '"+url_hash+"' to source almeshkat")
						break
				if url_hash == src_hash: #we have reached the old links stop the loop
					break
				else:
					yield scrapy.Request(url, callback=self.parse_books_contents)
			i = i+1

	def parse_books_contents(self, response):
		category = ""
		if response.xpath('//td/span/a[@href="index.php"]/following-sibling::a/text()'):
			category = response.xpath('//td/span/a[@href="index.php"]/following-sibling::a/text()').extract()[0].encode('utf-8','ignore')
		
		name = ""
		author = ""
		description = ""
		download_url = ""
		document_url = response.url

		tds =  response.xpath('//td/span/a[@href="index.php"]/parent::span/parent::td/parent::tr/following-sibling::tr/td[@width="10%"]')
		for td in tds:
			label = td.xpath('font/text()').extract()[0].encode('utf-8','ignore')
			if "التحميل" in label:
				download_url = td.xpath('following-sibling::td/a/@href').extract()[0].encode('utf-8','ignore')

			if "نبذه" in label:
				description = td.xpath('following-sibling::td/font/p/text()').extract()[0].encode('utf-8','ignore')
				f = open("azer.txt","w")
				f.write(description)
				f.close()

			if td.xpath('following-sibling::td/font/text()'):
				value = td.xpath('following-sibling::td/font/text()').extract()[0].encode('utf-8','ignore')
				if "العنوان" in label:					
					name = value
				if "المؤلف" in label:
					author = value

		item = AlmeshkatItem()
		item['category'] = category
		item['name'] = name
		item['author'] = author
		item['description'] = description
		item['ahu_download_url'] = "pending"
		item['document_url'] = document_url
		item['src_download_url'] = download_url

		store_in_es("almeshkat.net",item,False)