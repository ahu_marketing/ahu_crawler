# -*- coding: utf-8 -*-

import scrapy
from crawler.items import IslamHouseItem
import redis
import hashlib
import logging
import datetime
import json
from crawler.es import store_in_es

class IslamHouseSpider(scrapy.Spider):
	name = "islamhouse_en"
	# allowed_domains = ["iefpedia.com"]
	start_urls = [
		"http://islamhouse.com/en/api/content/get-list/en/showall/books/1/"
	]

	def parse(self, response):

		d = response.body_as_unicode()
		d = json.loads(d)
		
		meta = d['meta']
		pages = meta['pages']
		data = d['data']

		# check if we need to check other pages (redis)
		stop_index = False
		
		r = redis.StrictRedis(host='localhost', port=6379, db=8, socket_connect_timeout=2)
		src_hash = r.get('islamhouse.en:recent')

		i = 0
		for d in data:
			url = "http://islamhouse.com/en/books/"+str(d['id'])+"/"
			url_hash = hashlib.md5(url).hexdigest()
			if i == 0 and url_hash == src_hash: #no new links has been found
				stop_index = d['id']
				break
			else: #there are new links
				if i == 0: #save the hash of first link
					if not r.set('islamhouse.en:recent',url_hash):
						logging.warning(now+" couldnt set the hash '"+url_hash+"' to source islamhouse.en")
						break
				
				if url_hash == src_hash: #we have reached the old links stop the loop
					stop_index = d['id']
			i = i+1

		self.parse_content(data,stop_index)

		if stop_index == False:
			for x in xrange(2,int(pages)):
				url = "http://islamhouse.com/en/api/content/get-list/en/showall/books/"+str(x)+"/"
				# loop through pages
				yield scrapy.Request(url, callback=self.parse_pages_content)

	def parse_pages_content(self, response):
		#parse content
		d = response.body_as_unicode()
		d = json.loads(d)
		data = d['data']
		self.parse_content(data,False)

	def parse_content(self,data,stop_index):
		items = []
		for d in data:
			id = d['id']
			if stop_index == id:
				break

			item = IslamHouseItem()
			
			download_link = False

			if 'attachments' in d:
				download_link = d['attachments'][0]['url']
				item['category'] = d['type']
				title = ""
				if 'title' in d:
					title = d['title']
				item['name'] = title 
				author = ""
				if 'prepared_by' in d:
					if type(d['prepared_by']).__name__ != "NoneType":
						if 'authors' in d['prepared_by']:
							if 'author' in d['prepared_by']['authors']:
								author = d['prepared_by']['authors']['author'][0]['title']
						elif 'sources' in d['prepared_by']:
							author = d['prepared_by']['sources'][0]['title']

				item['author'] = author
				description = ""
				if d['full_description'] != "":
					description = d['full_description']
				else:
					description = d['description']
				item['description'] = description
				item['ahu_download_url'] = "pending"
				item['document_url'] = "http://islamhouse.com/en/books/"+str(id)+"/"
			
				item['src_download_url'] = download_link

			if download_link != False:
				items.append(item)

		# check if items has any item in it
		if len(items)>0:
			# store links to es
			store_in_es("islamhouse.com/en/",items,True)