# -*- coding: utf-8 -*-

import scrapy
from crawler.items import KantakjiItem
import redis
import hashlib
import logging
import datetime
from crawler.es import store_in_es

logging.basicConfig(filename='../ahu_crawler.log',level=logging.WARNING)
now = str(datetime.datetime.now())

class KantakjiSpider(scrapy.Spider):
	name = "kantakji_recent"
	allowed_domains = ["kantakji.com"]
	start_urls = [
		"http://kantakji.com/"
	]

	def parse(self, response):
		r = redis.StrictRedis(host='localhost', port=6379, db=8, socket_connect_timeout=2)
		i = 0
		links = response.xpath('//a[@class="siteRecentLinks "]')
		src_hash = r.get('kantakji:recent')
		for link in links:
			link = link.xpath('@href').extract()[0]
			url = "http://kantakji.com"+link.encode('utf-8','ignore').strip()
			url_hash = hashlib.md5(url).hexdigest()
			if i == 0 and url_hash == src_hash: #no new links has been found
				break
			else: #there are new links
				if i == 0: #save the hash of first link
					if not r.set('kantakji:recent',url_hash):
						logging.warning(now+" couldnt set the hash '"+url_hash+"' to source kantakji")
						break
				
				if url_hash == src_hash: #we have reached the old links stop the loop
					break
				else:
					yield scrapy.Request(url, callback=self.parse_links_content)
			i = i+1

	def parse_links_content(self, response):
		category = response.xpath('//div[@id="title"]/a/h1/text()').extract()[0].encode('utf-8','ignore').strip()		
		store = True
		item = KantakjiItem()
		item['category'] = category
		item['name'] = response.xpath('//div[@id="article_banner_title"]/h1/text()').extract()[0].encode('utf-8','ignore').strip()
		item['author'] = response.xpath('//div[@id="articleContent"]/h2/text()').extract()[0].encode('utf-8','ignore').strip()
		item['description'] = ""
		item['document_url'] = response.url.encode('utf-8','ignore').strip()
		item['ahu_download_url'] = "pending"
		if response.xpath('//a[@id="article_download"]/@href'):
			item['src_download_url'] = "http://kantakji.com"+response.xpath('//a[@id="article_download"]/@href').extract()[0].encode('utf-8','ignore').strip()
		else:
			store = False
		if store:
			store_in_es("kantakji.com",item,False)