# -*- coding: utf-8 -*-

import scrapy
from crawler.items import AlmaktabaItem
import redis
import hashlib
import logging
import datetime
from crawler.es import store_in_es

logging.basicConfig(filename='../ahu_crawler.log',level=logging.WARNING)
now = str(datetime.datetime.now())

class AlmaktabaSpider(scrapy.Spider):
	name = "almaktaba_recent"
	allowed_domains = ["almaktba.com"]
	start_urls = [
		"http://www.almaktba.com/"
	]

	def parse(self, response):
		r = redis.StrictRedis(host='localhost', port=6379, db=8, socket_connect_timeout=2)
		i = 0
		link_nodes = response.xpath('//td[@width="48%"]/table/tr[2]/td/table/tr/td[2]/a')
		src_hash = r.get('almaktaba:recent')
		for link_node in link_nodes:
			url = "http://almaktaba.com"+link_node.xpath('@href').extract()[0].encode('utf-8','ignore').strip()
			name = link_node.xpath('text()').extract()[0].encode('utf-8','ignore').strip()
			url_hash = hashlib.md5(url).hexdigest()
			if i == 0 and url_hash == src_hash: #no new links has been found
				break
			else: #there are new links
				if i == 0: #save the hash of first link
					if not r.set('almaktaba:recent',url_hash):
						logging.warning(now+" couldnt set the hash '"+url_hash+"' to source almaktaba")
						break
				
				if url_hash == src_hash: #we have reached the old links stop the loop
					break
				else:
					#store

					category = ""
					item = AlmaktabaItem()
					item['category'] = category
					item['name'] = name
					item['author'] = ""
					item['description'] = ""
					item['document_url'] = response.url.encode('utf-8','ignore').strip()
					item['ahu_download_url'] = "pending"
					item['src_download_url'] = url

					store_in_es("almaktaba.com",item,False)
			i = i+1
