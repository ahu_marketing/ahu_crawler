# -*- coding: utf-8 -*-

import scrapy
from crawler.items import WaqfeyaItem
from crawler.es import store_in_es

class WaqfeyaSpider(scrapy.Spider):
	name = "waqfeya"
	allowed_domains = ["waqfeya.com"]
	start_urls = [
		"http://waqfeya.com/"
	]

	def parse(self, response):
		lis = response.xpath('//td[@class="row1"][2]/span[@class="postbody"]/ul/li')
		for li in lis:
			if li.xpath('ul'): #category has sub cats => take only the sub categories
				parent_a = li.xpath('a')
				parent_url = "http://waqfeya.com/"+parent_a.xpath('@href').extract()[0].encode('utf-8','ignore') # main category url
				if "cid=32" in parent_url:
					url = "http://waqfeya.com/category.php?cid=109" # take only english version
					yield scrapy.Request(url, callback=self.parse_categories_contents)
				else:
					sub_lis = li.xpath('ul/li')
					for li in sub_lis:
						a = li.xpath('a')
						for elem in a:
							url = "http://waqfeya.com/"+elem.xpath('@href').extract()[0].encode('utf-8','ignore')
							yield scrapy.Request(url, callback=self.parse_categories_contents)

			elif li.xpath('a'):
				a = li.xpath('a')
				for elem in a:
					url = "http://waqfeya.com/"+elem.xpath('@href').extract()[0].encode('utf-8','ignore')
					if "cid=104" in url or "cid=105" in url or "cid=106" in url or "cid=130" in url or "cid=151" in url or "cid=144" in url or "cid=157" in url or "cid=160" in url or "cid=161" in url or "cid=34" in url or "cid=158" in url : #skip these urls , they are non relevant
						pass
					else:
						yield scrapy.Request(url, callback=self.parse_categories_contents)

	def parse_categories_contents(self, response):
		rows = response.xpath('//tr[@valign="top"]/td[2]/table')
		tables_count = len(rows)/3 
		j = 0
		items = []
		td = response.xpath('//tr[@valign="top"]/td[2]')
		pagination = False
		if response.xpath('//td[@class="row1" and @valign="top"][last()]/span/div/a'):
			pagination = True
			tables_count = tables_count - 1

		category = response.xpath('//title/text()').extract()[0].encode('utf-8','ignore')
		category = category.replace("- المكتبة الوقفية للكتب المصورة PDF","")

		for i in xrange(1,tables_count+1): #loop through the documents
			j = i*3-2 # one single document data is distributed through 3 tables
			description = ""
			table1 = td.xpath('table['+str(j)+']')
			a = table1.xpath('tbody/tr/td[2]/span/a')
			document_url = ""
			if a.xpath('@href'):
				document_url = "http://waqfeya.com/"+a.xpath('@href').extract()[0].encode('utf-8','ignore')
			name = ""
			if a.xpath('text()'):
				name = a.xpath('text()').extract()[0].encode('utf-8','ignore')

			j = j+1
			table2 = td.xpath('table['+str(j)+']')
			a = table2.xpath('tbody/tr/td/span/ul/li[last()]/a[1]')
			dld_title = a.xpath('text()').extract()[0].encode('utf-8','ignore')
			download_url = a.xpath('@href').extract()[0].encode('utf-8','ignore')
			downloadable = False
			if "cid=109" in response.url: #english version
				if "Save Target As" in dld_title or "save target as" in dld_title: #if this is a download link
					downloadable = True
			else: #arabic
				if "تحميل" in dld_title or ("الكتاب" in dld_title and (".pub" in download_url or ".chm" in download_url or ".doc" in download_url) ): #if this is a download link
					downloadable = True
			
			if downloadable:
				#check for the author
				lis = table2.xpath('tbody/tr/td/span/ul/li')
				author = ""
				for li in lis:
					li_content = li.xpath('text()').extract()[0].encode('utf-8','ignore')
					if "المؤلف" in li_content:
						author = li_content.replace("المؤلف :","")
						author = li_content.replace("المؤلف:","")
						author = author.strip()

				item = WaqfeyaItem()
				item['category'] = category
				item['name'] = name
				item['author'] = author
				item['description'] = description
				item['ahu_download_url'] = "pending"
				item['document_url'] = document_url
				item['src_download_url'] = download_url
				items.append(item)

		# check if items has any item in it
		if len(items)>0:
			# store links to es
			store_in_es("waqfeya.com",items,True)

		# if there is next page we want to crawl it too
		if not "&st=" in response.url:
			if pagination: 
				links = response.xpath('//td[@class="row1" and @valign="top"][last()]/span/div/a')
				for link in links:
					next_page_url = "http://waqfeya.com/"+link.xpath('@href').extract()[0].encode('utf-8','ignore')
					yield scrapy.Request(next_page_url, callback=self.parse_categories_contents) #parse deeper category