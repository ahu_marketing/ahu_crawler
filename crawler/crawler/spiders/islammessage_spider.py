# -*- coding: utf-8 -*-

import scrapy
from crawler.items import IslamMessageItem
import redis
import hashlib
import logging
import datetime
from crawler.es import store_in_es

logging.basicConfig(filename='../ahu_crawler.log',level=logging.WARNING)
now = str(datetime.datetime.now())

class IslamMessageSpider(scrapy.Spider):
	name = "islammessage"
	start_urls = [
		"http://islamicstudies.islammessage.com/Elibrary.aspx"
	]

	def parse(self, response):

		r = redis.StrictRedis(host='localhost', port=6379, db=8, socket_connect_timeout=2)

		trs = response.xpath('//div[@id="Researchtab"]/table/tr/td/table/tr')[1:]
		for tr in trs:
			cat = tr.xpath('td[1]/a/text()').extract()[0].encode('utf-8','ignore').strip()
			url = tr.xpath('td[1]/a/@href').extract()[0].encode('utf-8','ignore').strip()
			updated = tr.xpath('td[3]/text()').extract()[0].encode('utf-8','ignore').strip()
			old_index = r.get("islammessage:"+cat+":index")
			r.set("islammessage:"+cat+":index",updated)

			#check if there is a new data
			if old_index != updated:
				url = "http://islamicstudies.islammessage.com/"+url
				yield scrapy.Request(url, callback=self.parse_categories_contents)

	def parse_categories_contents(self, response):
		cat = response.xpath('//span[@class="small2"]/text()').extract()[0].split()[0]
		trs = response.xpath('//div[@id="Researchtab"]/table/tr/td/table/tr')[1:]
		r 	= redis.StrictRedis(host='localhost', port=6379, db=8, socket_connect_timeout=2)
		i 	= 0
		src_hash = r.get('islammessage:'+cat+':books:index')
		for tr in trs:
			if tr.xpath('td[1]/a/@href').extract():
				if not "javascript" in tr.xpath('td[1]/a/@href').extract()[0]:
					url = "http://islamicstudies.islammessage.com/"+tr.xpath('td[1]/a/@href').extract()[0].encode('utf-8','ignore').strip()
					url_hash = hashlib.md5(url).hexdigest()
					if i == 0 and url_hash == src_hash: #no new links has been found
						break
					else: #there are new links
						if i == 0: #save the hash of first link
							if not r.set('islammessage:'+cat+':books:index',url_hash):
								logging.warning(now+" couldnt set the hash '"+url_hash+"' to source almaktaba")
								break
						if url_hash == src_hash: #we have reached the old links stop the loop
							break
						else:
							yield scrapy.Request(url, callback=self.parse_book_contents)
					i = i+1


	def parse_book_contents(self,response):
		cat =  response.xpath('//span[@class="audiolink2"]/a[last()]/text()').extract()[0].encode('utf-8','ignore').strip()
		tds = response.xpath('//div[@id="Researchtab"]/table/tr[1]/td/table/tr/td[2]')
		title = tds[0].xpath('text()').extract()[0].encode('utf-8','ignore').strip()
		author = tds[1].xpath('text()').extract()[0].encode('utf-8','ignore').strip()
		url = response.xpath('//img[@src="images/download.gif"]/parent::a/@href').extract()[0].encode('utf-8','ignore').strip()
		if not "http://islamicstudies.islammessage.com" in url:
			url = "http://islamicstudies.islammessage.com"+url

		item = IslamMessageItem()
		item['category'] = cat
		item['name'] = title
		item['author'] = author
		item['description'] = ""
		item['document_url'] = response.url.encode('utf-8','ignore').strip()
		item['ahu_download_url'] = "pending"
		item['src_download_url'] = url

		store_in_es("islammessage.com",item,False)