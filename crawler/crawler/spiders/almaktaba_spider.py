# -*- coding: utf-8 -*-

import scrapy
from crawler.items import AlmaktabaItem
from crawler.es import store_in_es

class AlmaktabaSpider(scrapy.Spider):
	name = "almaktaba"
	allowed_domains = ["almaktba.com"]
	start_urls = [
		"http://www.almaktba.com/"
	]

	next_page_urls = [] #this variable holds urls that are for next pages ( categories in these pages shouldn't parse )

	def parse(self, response):
		rows = response.xpath('//td[@valign="TOP"]/table[2]/tr/td/table/tr[last()]/td/table/tr')
		for row in rows:
			tds = row.xpath('td')
			for td in tds:
				if td.xpath('a/text()'):
					url = "http://www.almaktba.com"+td.xpath('a/@href').extract()[0]
					if not "/index.php?cid=167" in url: #skip this categorry as it is non relevant
						yield scrapy.Request(url, callback=self.parse_categories_contents)

	def parse_categories_contents(self, response):
		if response.xpath('//a[@class="xLink"]'): # if there are books
			category =  response.xpath('//td[@bgcolor="#708090"]/font[@class="head"]/img[last()]/following-sibling::text()[1]').extract()[0].encode('utf-8','ignore').split()
			book_rows = response.xpath('//a[@class="xLink"]/parent::td/parent::tr')
			items = []
			for book_row in book_rows: #parse rows
				author = book_row.xpath('td[2]/text()').extract()[0].encode('utf-8','ignore').split()
				book_data = book_row.xpath('td[1]')
				name = book_data.xpath('a[@class="xLink"]/text()').extract()[0].encode('utf-8','ignore').split()
				description = book_data.xpath('font/text()').extract()[0].encode('utf-8','ignore').strip()+book_data.xpath('font/following-sibling::text()[1]').extract()[0].encode('utf-8','ignore').strip()
				download_url = "http://www.almaktba.com"+book_data.xpath('a[@class="xLink"]/@href').extract()[0].encode('utf-8','ignore').split()

				item = AlmaktabaItem()
				item['category'] = category
				item['name'] = name
				item['author'] = author
				item['description'] = description
				item['ahu_download_url'] = "pending"
				item['document_url'] = response.url
				item['src_download_url'] = download_url
				items.append(item)

			# check if items has any item in it
			if len(items)>0:
				# store links to es
				store_in_es("almaktaba.com",items,True)

			if response.xpath('//tr[@valign="top"]/td[@align="center"]/a'): #if there is next page we want to crawl it too
				next_url_label = response.xpath('//tr[@valign="top"]/td[@align="center"]/a[1]/text()').extract()[0]
				if "التالي" in next_url_label:
					next_page_url = "http://www.almaktba.com"+response.xpath('//tr[@valign="top"]/td[@align="center"]/a[1]/@href')[0]
					next_page_urls.append(next_page_url)
					yield scrapy.Request(next_page_url, callback=self.parse_categories_contents) #parse deeper category
					
		if response.url not in next_page_urls: #only if this is not a "next page"
			if response.xpath('//a[@class="secTitle"]'): # if there deeper categories
				deeper_categories = response.xpath('//a[@class="secTitle"]')
				for cat in deeper_categories:
					cat_url = "http://www.almaktba.com"+cat.xpath('@href').extract()[0]
					yield scrapy.Request(cat_url, callback=self.parse_categories_contents) #parse deeper category