# -*- coding: utf-8 -*-

import scrapy
from crawler.items import IslamPortItem
from crawler.es import store_in_es

class IslamPortSpider(scrapy.Spider):
	name = "islamport"
	allowed_domains = ["islamport.com"]
	start_urls = [
		"http://islamport.com/index2.html"
	]

	def parse(self, response):
		categories = response.xpath('//table[@id="table8"]/descendant::a/@href').extract()
		for category in categories:
			url = category
			yield scrapy.Request(url, callback=self.parse_categories_contents)

	def parse_categories_contents(self, response):
		category = response.xpath('body/p[2]/font/text()').extract()[0].encode('utf-8','ignore').replace('كتب','').strip()
		rows = response.xpath('body/div[last()]/table/tr')
		items = []
		rows = rows[1:]
		for row in rows:
			item = IslamPortItem()

			book_title = row.xpath('td[1]/font/ul/b/font/text()').extract()[0].encode('utf-8','ignore').strip()
			book_meta_data = row.xpath('td[1]/font/ul/ul/div').extract()[0].encode('utf-8','ignore').strip()

			book_meta_data = book_meta_data.replace('<div align="right" dir="rtl">','')
			book_meta_data = book_meta_data.replace('</div>','')
			book_meta_data = book_meta_data.split("<br>")

			description = '<br />'.join(book_meta_data).strip()

			author = ""
			if len(book_meta_data)>2: #extract the author name
				for m in book_meta_data:
					if "المؤلف" in m:
						author = m.replace("المؤلف :","")
						author = m.replace("المؤلف:","")
						author = author.strip()
						description = description.replace("المؤلف :"+author,"").strip()

			download_link = response.url+row.xpath('td[last()]/p/a/@href').extract()[0].encode('utf-8','ignore')
			item['category'] = category
			item['name'] = book_title
			item['author'] = author
			item['description'] = description
			item['ahu_download_url'] = "pending"
			item['document_url'] = response.url
			item['src_download_url'] = download_link
			items.append(item)

		# check if items has any item in it
		if len(items)>0:
			# store links to es
			store_in_es("islamport.com",items,True)