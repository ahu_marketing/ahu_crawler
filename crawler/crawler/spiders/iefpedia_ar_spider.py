# -*- coding: utf-8 -*-

import scrapy
from crawler.items import IefpediaItem
import redis
import hashlib
import logging
import datetime
from crawler.es import store_in_es
import urllib

class IefpediaSpider(scrapy.Spider):
	name = "iefpedia_ar"
	allowed_domains = ["iefpedia.com"]
	start_urls = [
		"http://iefpedia.com/arab/category/%D8%A7%D9%84%D8%A3%D9%82%D8%B3%D8%A7%D9%85-%D8%A7%D9%84%D8%B1%D8%A6%D9%8A%D8%B3%D9%8A%D8%A9"
	]

	def parse(self, response):
		if "/page/" not in response.url:
			last_page = False
			if response.xpath('//div[@id="wp_page_numbers"]/ul/li[@class="first_last_page"]/a/text()'):
				last_page = int(response.xpath('//div[@id="wp_page_numbers"]/ul/li[@class="first_last_page"]/a/text()').extract()[0].strip())

			if last_page != False:
				r = redis.StrictRedis(host='localhost', port=6379, db=8, socket_connect_timeout=2)
				src_hash = r.get('iefpedia.ar:recent')
				if src_hash == None: # if this is first time crawling
					for page in xrange(1,last_page+1): # we request other pages only when we are at the first page
						if page == 1:
							url = "http://iefpedia.com/arab/category/%D8%A7%D9%84%D8%A3%D9%82%D8%B3%D8%A7%D9%85-%D8%A7%D9%84%D8%B1%D8%A6%D9%8A%D8%B3%D9%8A%D8%A9"
						else:
							url = "http://iefpedia.com/arab/category/%D8%A7%D9%84%D8%A3%D9%82%D8%B3%D8%A7%D9%85-%D8%A7%D9%84%D8%B1%D8%A6%D9%8A%D8%B3%D9%8A%D8%A9/page/"+str(page)
						yield scrapy.Request(url, callback=self.parse_page_content)
		else: # if this page is not the first page => pass it to next step
			yield scrapy.Request(response.url, callback=self.parse_page_content)

	def parse_page_content(self, response):
		urls =  response.xpath('//span[@class="title"]/a/@href').extract()
		if "/page/" not in response.url:
			r = redis.StrictRedis(host='localhost', port=6379, db=8, socket_connect_timeout=2)
			src_hash = r.get('iefpedia.ar:recent')
			i = 0
			for url in urls:
				url_hash = hashlib.md5(url).hexdigest()
				if i == 0 and url_hash == src_hash: #no new links has been found
					break
				else: #there are new links
					if i == 0: #save the hash of first link
						if not r.set('iefpedia.ar:recent',url_hash):
							logging.warning(now+" couldnt set the hash '"+url_hash+"' to source iefpedia.ar")
							break
					
					if url_hash == src_hash: #we have reached the old links stop the loop
						break
					else:
						yield scrapy.Request(url, callback=self.parse_books_content)
				i = i+1
		else: # we don't want redis to track other pages than front page
			for url in urls:
				yield scrapy.Request(url, callback=self.parse_books_content)

	def parse_books_content(self, response):
		author = ""
		category = ""
		description = ""
		main = response.xpath('//div[@class="column_main"]')
		if main.xpath('h1/text()'):
			name = main.xpath('h1/text()').extract()[0].encode('utf-8','ignore').strip()
			if  main.xpath('p/a[@rel="category"]/text()'):
				category = main.xpath('p/a[@rel="category"]/text()').extract()[0].encode('utf-8','ignore')
			# POST WRITTER
			# if  main.xpath('p/i/a[@rel="author"]/text()'):
			# 	author =  main.xpath('p/i/a[@rel="author"]/text()').extract()[0].encode('utf-8','ignore')

			details = main.xpath('.//div[@id="entry"]')
			possible_donwload_urls = details.xpath('.//a/@href').extract()
			download_link = False
			if len(possible_donwload_urls)>0:
				for url in possible_donwload_urls:
					if (".pdf" or ".doc" or ".ppt" or ".chm") in url:
						download_link = url

			if download_link != False:
				item = IefpediaItem()
				item['category'] = category
				item['name'] = name
				item['author'] = author
				item['description'] = description
				item['ahu_download_url'] = "pending"
				item['document_url'] = urllib.unquote(response.url)
				item['src_download_url'] = download_link

				store_in_es("iefpedia.com/arab",item,False)